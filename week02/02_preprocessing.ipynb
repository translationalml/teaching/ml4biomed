{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "be5ce44b",
   "metadata": {},
   "source": [
    "Preprocessing\n",
    "=============\n",
    "\n",
    "In this notebook you will learn how to preprocess your data to make it more suitable for machine learning.\n",
    "\n",
    "After completing this notebook, you should be able to\n",
    "* Properly rescale your continuous variables, including considering leakage across cross-validation folds\n",
    "* Encode categorical variables for algorithms that need them\n",
    "* Implement simple (but potentially dangerous) solutions for missing data\n",
    "\n",
    "You need to **complete and run** the code blocks that contain a WRITE comment, and if you have time left you can complete the BONUS parts."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "895aaf9e",
   "metadata": {},
   "source": [
    "---\n",
    "# Setup\n",
    "\n",
    "\n",
    "Let's start by loading the Cleveland Heart Disease data you used previously.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4489945f",
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "import seaborn as sns\n",
    "\n",
    "\n",
    "# WRITE pandas code to read your dataframe 'cleveland_df_cleaned.pkl' into a variable called dfc\n",
    "# HINT on Nuvolos, data will be in /files/data\n",
    "dfc=pd.read_pickle(XXX) # XXX\n",
    "\n",
    "if not np.all(dfc.shape==(297,15)):\n",
    "    raise RuntimeError('Dataframe does not have the expected dimension. Should be (297,15)')\n",
    "\n",
    "print(f\"Data shape: {dfc.shape}\")\n",
    "dfc.info()\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e259f463",
   "metadata": {},
   "source": [
    "SELF-CHECK: If all is well you have a 297x15 dataframe, with exclusively float64 (numerical) and category (categorical) variables, and no missing data."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a14f828a",
   "metadata": {},
   "source": [
    "---\n",
    "# Numerical variables\n",
    "\n",
    "As you've seen when fitting regularized regression models, having different scales for your variables can be problematic in terms of optimization convergence (e.g. Hessian inversion). Here we will cover two main approaches:\n",
    "- Standarization - here we transform each variable to have zero mean and unit standard deviation (also called z-normalization)\n",
    "- Rescaling - here we limit the range of the data to some chosen interval, such as $[0,1]$\n",
    "\n",
    "\n",
    "Featurewise (columnwise) standarization or rescaling has several impacts:\n",
    "- Interpretabilty: if your features have 'natural' units like cm or kg, interpretability may be reduced. On the other hand, rescaling to a common range means that (if features are at least on the same [level of measurement](https://en.wikipedia.org/wiki/Level_of_measurement)) model coefficients can be inspected to gain insights into feature importance.\n",
    "- Easier regularization: having a common scale for all features means that imposing a penalty on the norm of a coefficient vector will not be more detrimental to features that are small numerically\n",
    "- Change in solution: Of the algorithms we will see in this course, only decision trees will yield the same solution if the inputs are rescaled monotonically. Both logistic regression and support vector machines will yield a different solution.\n",
    "\n",
    "\n",
    "## Standardization and rescaling \n",
    "\n",
    "Let's start by standardizing a single column. We will use the sklearn [`StandardScaler`](https://scikit-learn.org/stable/modules/generated/sklearn.preprocessing.StandardScaler.html?highlight=standardscaler#sklearn.preprocessing.StandardScaler) class, and focus on `Age`.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "40781158",
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.preprocessing import StandardScaler\n",
    "\n",
    "# make numpy array to keep StandardScaler happy\n",
    "tmp_Age_np=dfc['Age'].to_numpy().reshape(-1,1)\n",
    "print(f\"Original Age variable has shape {tmp_Age_np.shape}, mean {np.mean(tmp_Age_np):0.2f}, \" +\n",
    "      f\"and standard deviation {np.std(tmp_Age_np):0.2f}\")\n",
    "\n",
    "# WRITE sklearn code to instantiate the StandardScaler class. Call this my_scaler.\n",
    "my_scaler = XXX\n",
    "\n",
    "# WRITE sklearn code to learn the transform on tmp_Age_np using .fit()\n",
    "XXX\n",
    "\n",
    "print(f\"Standard Scaler fitted mean: {my_scaler.mean_}, sd: {my_scaler.scale_}\")\n",
    "\n",
    "# WRITE sklearn code to apply the transform (.transform) to the same data, storing results in tmp_Age_z\n",
    "tmp_Age_z=XXX\n",
    "\n",
    "# regroup in a temp df with nice names for plotting\n",
    "tmp_df=pd.DataFrame(np.hstack((tmp_Age_np, tmp_Age_z)), columns=['original','standardized'])\n",
    "\n",
    "# jointplot() the distribution of original age and standardized age using tmp_df\n",
    "plt.figure()\n",
    "p=sns.jointplot(x='original', y='standardized', data=tmp_df)\n",
    "plt.suptitle('Original vs standardized age',y=1)\n",
    "\n",
    "# NOTE you can also fit and transform in a single step\n",
    "tmp_Age_z_2=my_scaler.fit_transform(tmp_Age_np)\n",
    "print(f\"fit_transform() matches fit() then transform(): {np.all(tmp_Age_z_2==tmp_Age_z)}\")\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "aeacb229",
   "metadata": {},
   "source": [
    "SELF-CHECK: Your Age mean and sd should be `mean: [54.54208754], sd: [9.03448759]`.\n",
    "\n",
    "SELF-CHECK: You should also see `fit_transform() matches fit() then transform(): True`\n",
    "\n",
    "Look at the range on both axes, and at the marginal histograms.\n",
    "\n",
    "Now, let's industrialize the process and do all numerical columns. As before we'll make a sub-dataframe containing only numerical features.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b287d37a",
   "metadata": {},
   "outputs": [],
   "source": [
    "# keep only columns with numerical dtypes\n",
    "X_num=dfc.select_dtypes('number')\n",
    "X_num.info()\n",
    "\n",
    "# Fit and transform dfc_num in one go, saving results in a new dataframe called X_num_z\n",
    "# The scaler gives back a numpy array, so we need to create a new pandas DataFrame from it\n",
    "# we also directly name the columns using the columns argument\n",
    "X_num_z=pd.DataFrame(my_scaler.fit_transform(X_num), columns=X_num.columns.values)\n",
    "\n",
    "# inspect results\n",
    "X_num_z.describe()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e2ed79ef",
   "metadata": {},
   "source": [
    "Inspect the results. All means should be nearly 0, and the sd should be very close to 1.\n",
    "\n",
    "Great. As you see a one-liner is sufficient, now we're ready to try regularized regression again!\n",
    "\n",
    "...\n",
    "\n",
    "Not so fast. **We just commited a very common mistake here**.\n",
    "\n",
    "From what you know about the need to train and test on disjoint subsets of the data, can you guess what the mistake is?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3ef7dd54",
   "metadata": {},
   "source": [
    "## Need for proper train/test separation across the whole ML pipeline\n",
    "\n",
    "We just learned the mean and sd on the whole sample! We should (almost) never do this - the distribution of \n",
    "test set features is is extra information that we should not have access to at training time.\n",
    "\n",
    "This is a very, very common mistake. If your sample is very large and well behaved distributionally this may not be so bad, but most biomedical datasets are not so large and not so nicely distributed, e.g. non-Gaussian, heavy-tailed, and with outliers.\n",
    "\n",
    "Instead, the proper way to do this is to `.fit()` on training data, and to `.transform()` on test data. We know how to do this already!\n",
    "\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7e24d08b",
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.model_selection import KFold\n",
    "from sklearn.linear_model import LogisticRegression\n",
    "from sklearn.metrics import roc_auc_score, confusion_matrix\n",
    "\n",
    "# for indexing syntax simplicity, convert to numpy. We could work with pandas df too.\n",
    "X_num_np=dfc.select_dtypes('number').to_numpy()\n",
    "y=dfc.Class\n",
    "\n",
    "\n",
    "# let's do 10-fold CV again\n",
    "my_kf=KFold(n_splits=10, shuffle=True, random_state=42)\n",
    "\n",
    "# Let's do LR with some light l2 regularization\n",
    "#my_LR=LogisticRegression(penalty='l1', solver='liblinear', C=1e-1) # BONUS: try this later\n",
    "my_LR=LogisticRegression(penalty='l2', solver='lbfgs', C=0.1)\n",
    "\n",
    "# instantiate the scaler again for completeness\n",
    "my_scaler=StandardScaler()\n",
    "\n",
    "for tr_idx, te_idx in my_kf.split(X_num_np):\n",
    "    X_tr=X_num_np[tr_idx,:] # training data: pick all the rows corresponding to the training index in this fold\n",
    "    y_tr=y[tr_idx]       # training labels: pick all the rows corresponding to the training index in this fold\n",
    "    X_te=X_num_np[te_idx,:] # test data: pick all the rows corresponding to the test index in this fold\n",
    "    y_te=y[te_idx]       # test labels: pick all the rows corresponding to the test index in this fold\n",
    "    \n",
    "    # WRITE sklearn code to fit your scaler on training data for this fold (X_tr) and directly transform (fit_transform), saving result in X_tr_z\n",
    "    X_tr_z = XXX\n",
    "    \n",
    "    # WRITE sklearn code to transform test data (X_te) for this fold using your fitted scaler, saving results in X_te_z\n",
    "    X_te_z = XXX\n",
    "    \n",
    "    # now we fit an LR model on standardized training data for this fold\n",
    "    my_LR.fit(X_tr_z, y_tr)\n",
    "    print(my_LR.coef_)\n",
    "\n",
    "    # now we predict test class labels and probabilities for this fold, using the standardized test data\n",
    "    y_pred=my_LR.predict(X_te_z)\n",
    "    y_pred_p=my_LR.predict_proba(X_te_z)\n",
    "    \n",
    "    # Compute and print AUC and a confusion matrix\n",
    "    my_AUC=roc_auc_score(y_te,y_pred_p[:,1])\n",
    "    print(f\"AUC: {my_AUC:0.3f}\")\n",
    "    my_CM=confusion_matrix(y_te, y_pred)\n",
    "    print(my_CM)    "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "adc2e6b4",
   "metadata": {},
   "source": [
    "SELF-CHECK: First fold could give you something like this: \n",
    "    \n",
    "```\n",
    "[[ 0.09250268 -0.22122714 -0.1259024   0.60757306 -0.52931338 -0.83477695]]\n",
    "AUC: 0.861\n",
    "[[ 7  4]\n",
    " [ 2 17]]\n",
    "```\n",
    "\n",
    "At this point, you have properly scaled variables (without train/test leakage), proper regularization, and proper evaluation procedures in place. Well done! This is getting close to something you could publish if you applied it to your own data. What is still missing is proper tuning of hyperparameters, and of course critical analysis of possible confounding factors, maybe via post-hoc stratification.\n",
    "\n",
    "In any case, your are in a pretty good position now. What you gained by doing all of this is\n",
    "- Better judgement on the difficulty of the task. I'd say it's not a very difficult task (max AUC .95, min AUC .76).\n",
    "- Better judgement on the variance of your errors. \n",
    "- Better interpretability. Look at the weights for each feature. You should get a good sense for what does, or does not, predict narrowed major vessels in the Cleveland dataset.\n",
    "\n",
    "Altogether, fluoroscopy (misspelled in the variable name btw) seems to be a pretty good predictor of vessel narrowing, and indeed potentially a direct contributor to the diagnosis label. The task should be more difficult without this feature.\n",
    "\n",
    "BONUS: try an l1-regularized LR"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3611d845",
   "metadata": {},
   "source": [
    "## BONUS min-max rescaling\n",
    "\n",
    "Instead of standardizing, you can use the drop-in replacement [`sklearn.preprocessing.MinMaxScaler`](https://scikit-learn.org/stable/modules/generated/sklearn.preprocessing.MinMaxScaler.html#sklearn.preprocessing.MinMaxScaler).\n",
    "\n",
    "Try to rescale to 0-1 and see the effect on the regression."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "97feb7dc",
   "metadata": {},
   "source": [
    "--- \n",
    "# Discrete variables\n",
    "\n",
    "Discrete variables, also called categorical, qualitative variables, or factors, are typically associated with descriptions rather than numerical values. Examples include flower species, sex, smoking status, educational attainment (high school degree, bachelor's, master's...), portion size (small, large, medium), etc. Categorical variables can be *unordered* (sex, flower species, smoking status), in which case they are often called *nominal variables*, or *ordered* (educational attainment, portion size), in which case they are typically called *ordinal variables*.\n",
    "\n",
    "In Pandas, categorical variables are typically stored as interpretable strings (such as 'female', 'male'), but also have an underlying numerical code (such as 0,1) stored in unsigned integer. Let's quickly look at at this."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3ca61bdf",
   "metadata": {},
   "outputs": [],
   "source": [
    "# pick first few rows, show 'label' of Chest Pain Type\n",
    "print(dfc.iloc[0:10].Chest_Pain_Type)\n",
    "\n",
    "# print first few rows, show numerical 'code' of Chest Pain Type\n",
    "print(dfc.iloc[0:10].Chest_Pain_Type.cat.codes)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "145894a2",
   "metadata": {},
   "source": [
    "However, this code is not to be literally interpreted as a value - you cannot for instance multiply 'asymptomatic' chest pain type by 2.\n",
    "\n",
    "Of the ML algorithms we'll see in this course, only decision trees (e.g. random forests) can deal natively with both continuous and discrete variables. Therefore, it is necessary to transform discrete variables if you will be using another algorithm such as logistic regression or a support vector machine.\n",
    "\n",
    "The simplest way to do this is to expand each categorical variables from a single variable with $C$ different codes to binary representation, using a vector of dimension $C$ where only one dimension is 1 and all others are 0. This is called one-hot encoding, and is conceptually similar to dummy coding in statistics (in R many ways including `model.matrix()` or `psych::dummy.code()`).\n",
    "\n",
    "In scikit-learn, we want `sklearn.preprocessing.OneHotEncoder()`. in Pandas, `pd.get_dummies()`, which is illustrated below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "939ae19d",
   "metadata": {},
   "outputs": [],
   "source": [
    "# get names of all categorical variables except 'class'. These are the vars we want to encode.\n",
    "cat_vars=dfc.drop('Class', axis=1).select_dtypes('category').columns.values\n",
    "print(f\"Will encode {cat_vars}\")\n",
    "\n",
    "dfc_dc=pd.get_dummies(dfc, columns=cat_vars) # generate a dummy-coded version\n",
    "dfc_dc.info() # notice the expanded column names?\n",
    "\n",
    "plt.figure()\n",
    "sns.heatmap(dfc_dc.select_dtypes('bool'))\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a735c70b",
   "metadata": {},
   "source": [
    "And that's most of the preprocessing you need! The whole dataframe is now numerical (except for the Class variable which we can just `.drop()`), so you can do your usual routine - split into K folds, `.fit_transform()` a `StandardScaler()` on all columns of each fold's training set at once, `.transform()` each fold's test set, `.fit()` your ML classifier on the standardized training set, `.predict()`, and evaluate performance.\n",
    "\n",
    "You will already get a lot of mileage out of this simple pipeline.\n",
    "\n",
    "Speaking of, next notebook will introduce a few shortcut commands that will allow you to do cross-validation, preprocessing, training, testing, and performance computation with a single line of code.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3937495e",
   "metadata": {},
   "source": [
    "---\n",
    "# BONUS: Missing data\n",
    "\n",
    "Data collection can be costly and error-prone. Sensors can fail, people can forget to fill in a database field, or lack the time to ensure data integrity. With real data, in particular with clinical data, it is often the case that some variables will not be collected for all your samples.\n",
    "\n",
    "How to deal with missing data is a vast topic in statistics. One difficulty stems from the fact that data are often not missing at random - that is, whether a variable is present or not can depend on its unobserved value. Thus, how you handle missing data in your model can itself consistute a source of bias if the missingness mechanism is related to the outcome of interest in your model.\n",
    "\n",
    "We will only cover here the two most basic approaches - marginalization and univariate imputation.\n",
    "- Marginalization (or complete case analysis) simply removes whole samples that have missing variables.\n",
    "- Imputation tries to recover missing data by exploiting univariate statistics or multivariate correlations. For example, this is a routine technique in genetics, where not all genotyping arrays cover the same variants, but are designed on purpose with an imputation grid that makes it possible to increase coverage of non-genotyped variants through statistical imputation exploiting correlations between variants.\n",
    "\n",
    "Other approaches, like using models that can deal directly with missing data (CART / MARS / PRIM ...) are beyond the scope of this course.\n",
    "\n",
    "A very neat package if you want to go further is (missingno)[https://github.com/ResidentMario/missingno], but for basic approaches pandas and scikit-learn can already help.\n",
    "\n",
    "Overall, **handling missing data is a tricky topic** and generally should be given due consideration. \n",
    "\n",
    "\n",
    "## Marginalization\n",
    "\n",
    "This is generally quite easy to perform with Pandas, either using `df.dropna()` or indexing out rows that have undesirable values.\n",
    "\n",
    "For example, if a numerical variable has some missing data, e.g. denoted as the string \"?\" or \"NA\" (typically coming from a CSV file or Excel), Pandas will not recognize this as a numerical data  type. To illustrate a possible approach, you could replace by a `nan`, then transform to numerical dtype, then drop the cases with missing data. For example: \n",
    "\n",
    "```Python\n",
    "# replace \"?\"\" by a nan\n",
    "dfc=dfc.replace({\"Num_Major_Vessels_Flouro\": {\"?\": np.nan}})\n",
    "\n",
    "# Num_Major_Vessels_Flouro is still an 'object', so let's convert to a numerical dtype.\n",
    "# we could use an integer type as well\n",
    "dfc.Num_Major_Vessels_Flouro = dfc.Num_Major_Vessels_Flouro.astype(np.float64)\n",
    "\n",
    "# Finally, we drop cases where Num_Major_Vessels_Flouro is missing.\n",
    "dfc.dropna(inplace=True)\n",
    "dfc.reset_index(drop=True, inplace=True) # also reset the index so it is contiguous. inplace=True means we affect our dataframe directly\n",
    "\n",
    "```\n",
    "\n",
    "\n",
    "Scikit-learn can also help here, you can look at the doc for [impute.MissingIndicator](https://scikit-learn.org/stable/modules/generated/sklearn.impute.MissingIndicator.html#sklearn.impute.MissingIndicator) which you can use with a `ColumnTransformer`. However this is a bit more advanced."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "50e76a37",
   "metadata": {},
   "source": [
    "## Imputation\n",
    "\n",
    "A simple strategy is to replace missing values by the unconditional mean of this features in the other samples. In Scikit-learn, [impute.SimpleImputer](https://scikit-learn.org/stable/modules/generated/sklearn.impute.SimpleImputer.html#sklearn.impute.SimpleImputer) is easy to use.\n",
    "\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.13"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
