{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "9a4c9199",
   "metadata": {},
   "source": [
    "Fitting and regularizing logistic regression models for classification\n",
    "======================================================================\n",
    "\n",
    "In this notebook you will experiment with logistic regression for **classification** using $\\ell_1$, $\\ell_2$, and elastic net penalization.\n",
    "\n",
    "Given gene expression data for a tissue sample, our goal will be to predict whether the tissue sample came from the cortex of the cerebellum.\n",
    "\n",
    "After completing this notebook, you should be able to\n",
    "\n",
    "* Train a logistic regression classifier on a feature matrix\n",
    "* Predict class memberships given feature values\n",
    "* Inspect and discuss model parameters\n",
    "* Summarize the impact of various types of regularizers\n",
    "\n",
    "You need to **complete and run** the code blocks that contain a WRITE comment, and if you have time left you can complete the BONUS parts."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0bcfd6af",
   "metadata": {},
   "source": [
    "---\n",
    "# Background\n",
    "\n",
    "As you've read in \\[PRML\\] we will use the logistic sigmoid function as a building block. This is an improtant function in machine learning, including modern deep learning. A very nice property is that it maps $\\mathbb{R}\\to [0,1]$, which is one of the requirements in Kolmogorov's axioms for a value to be a valid probability.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f34af974",
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "\n",
    "my_x=np.linspace(-5,5,100)\n",
    "my_sigmoid=1/(1 + np.exp(-my_x))\n",
    "\n",
    "plt.plot(my_x, my_sigmoid)\n",
    "plt.title('Logistic sigmoid function')\n",
    "plt.grid()\n",
    "plt.xlabel('x')\n",
    "plt.ylabel('$\\sigma(x)$')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "88ed9dce",
   "metadata": {},
   "source": [
    "---\n",
    "# Exploratory data analysis\n",
    "\n",
    "As you know - no excuse for not looking !"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4c2fe9ef",
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "import numpy as np\n",
    "import seaborn as sns\n",
    "import matplotlib.pyplot as plt\n",
    "sns.set_theme(rc={\"figure.figsize\":(10, 8)}) #width=10, height=8 - change this to fit your needs\n",
    "\n",
    "\n",
    "# WRITE code to load the pickled pandas df containing the AIBS dataset (copy-paste from previous notebook)\n",
    "df=pd.read_pickle('/Users/Richiardi/_cloudStorage/switchDrive/teaching/ML4BioMed/data/week01/AIBS_br1_CXCB_494.pkl') # XXX\n",
    "\n",
    "my_features=['GABRA4','DOCK4']\n",
    "#my_features=['ORC2','DOCK4']\n",
    "#my_features=['ORC2','COIL']\n",
    "\n",
    "sns.jointplot(x=my_features[0], y=my_features[1], hue='class', data=df)\n",
    "\n",
    "X=df[my_features]                            # feature matrix: focus on just two genes for now\n",
    "y=(df['class'] == 'CX').astype('int32')      # labels: string -> boolean -> int32. Very useful trick!"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0e71c560",
   "metadata": {},
   "source": [
    "Looking at the joint plot, which gene is the most predictive of the tissue type?\n",
    "\n",
    "You can quickly look at a summary of what these genes do: [GABRA4](https://www.genecards.org/cgi-bin/carddisp.pl?gene=GABRA4) and\n",
    "[DOCK4](https://www.genecards.org/cgi-bin/carddisp.pl?gene=DOCK4) - do your results make sense biologically?\n",
    "\n",
    "Of course, I didn't pick GABRA4 at random given the task and what we know about GABAergic pathways in the brain - this is called a **hypothesis-driven approach** or applying **domain knowledge**.\n",
    "\n",
    "Try again with ORC2 and DOCK4, then with ORC2 and COIL. You can probably guess already which features provide the most **discriminative power** between cerebellum and cortex samples...\n",
    "\n",
    "Now, let's try to fit a classifier."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "680c532b",
   "metadata": {},
   "source": [
    "---\n",
    "# Low-dimensional case\n",
    "\n",
    "Here, we will again use the `sklearn.linear_model` package, this time the `LogisticRegression` class.\n",
    "\n",
    "## Soft predictions\n",
    "\n",
    "Classifier predictions can be given as \"soft\" predictions $f(\\mathbf{x}_i)$, which are positive or negative scalars. In the case of logistic regression, soft predictions are between 0 and 1 (see background section above), we have a direct probabilistic interpretation, and for each sample the sum of probabilities assigned to each class sums to one (again, Kolmogorov axioms).\n",
    "\n",
    "Soft predictions can be made \"crisp\" or \"hard\" by comparing the value to a threshold, for instance in the two-class case: $\\hat{\\omega}=\\delta(f(\\mathbf{x}_i) > \\tau)$, where $\\delta$ is the Kronecker indicator function and $\\tau$ is a suitably chosen threshold. Typically, $\\tau=0.5$ is chosen to correspond to the Bayes decision rule.\n",
    "\n",
    "Some classifiers such as the random forest give predictions directly in 'hard' form.\n",
    "\n",
    "Make sure to contrast the `predict_proba()` and `predict()` methods of the `LogisticRegression` class before proceeding."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "48752991",
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.linear_model import LogisticRegression\n",
    "\n",
    "# WRITE sklearn code to train a LogisticRegression classifier called clf on values of the GABRA4 and DOCK4 genes.\n",
    "# Your target is the class variable. Use the following options: solver='lbfgs', penalty=None\n",
    "clf_LR = LogisticRegression(solver=\"lbfgs\", random_state=123, penalty=None) # XXX\n",
    "clf_LR.fit(X,y)                                                               # XXX\n",
    "\n",
    "# Inspect the coefficients in clf_LR.coef_ (should be 3.45426702 1.62861763)\n",
    "print(clf_LR.coef_)\n",
    "\n",
    "# WRITE sklearn code to predict class probability for all samples in X, storing in y_pred_LR_p\n",
    "# Inspect the shape and confirm each row sums to 1\n",
    "y_pred_LR_p=clf_LR.predict_proba(X) # XXX\n",
    "print(y_pred_LR_p.shape)            # XXX\n",
    "print(np.sum(y_pred_LR_p,axis=1))   # XXX\n",
    "\n",
    "# WRITE seaborn code to swarmplot probabilities of belonging to first class for each sample (y axis), grouping by class label (x axis).\n",
    "# HINT recall the shape of y_pred_LR_p, and that swarmplot wants a vector not a matrix input\n",
    "plt.figure()\n",
    "sns.swarmplot(x=y, y=y_pred_LR_p[:,0]) # XXX\n",
    "\n",
    "# BONUS based on the probabilities, how would you quantify how good each classifier is in terms of separating classes? Write some code\n",
    "# to see if it works. One possibility is to use a submodule of scipy...\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b073b8d8",
   "metadata": {},
   "source": [
    "How separated are the probabilities?\n",
    "\n",
    "Repeat this analysis with the two other gene sets (ORC2+DOCK4, ORC2+COIL).\n",
    "\n",
    "Does this confirm your graphical intuition on discriminative power of each gene? Why or why not?\n",
    "\n",
    "Usual warning - take results with a large grain of salt because we are evaluating performance on the same dataset we used to train our model!"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fbe02c97",
   "metadata": {},
   "source": [
    "## Separability and the decision boundary in feature space\n",
    "\n",
    "Now, you may suspect that some samples are easier to classify than others. For instance a sample with low expression of both GABRA4 and DOCK4 is likely a cerebellum sample, but a sample with middling GABRA4 and DOCK4 levels may be more ambiguous. In probability terms, the soft decision should be closer to extremes (0 or 1) if the classifier is more certain of the class.\n",
    "\n",
    "Of course this is somewhat simplistic -  \"knowing that you don't know\" and quantifying predictive uncertainty is a whole field of research in ML, with specific learning methods including variational learning and MCMC approaches. See for instance [Der Kiureghian and Ditlevsen 2009](https://www.sciencedirect.com/science/article/pii/S0167473008000556) or [Kendall and Gal NeurIPS 2017](https://papers.nips.cc/paper/7141-what-uncertainties-do-we-need-in-bayesian-deep-learning-for-computer-vision.pdf).\n",
    "\n",
    "In any case, let's visualise class probabilities in feature space."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "10779296",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Make sure to re-set your feature space to GABRA4+DOCK4, retrain your model, and re-predict, otherwise this will be confusing\n",
    "\n",
    "# WRITE seaborn code to plot the feature space, with color as class label, and the size of each marker\n",
    "# proportional to the probability that this sample belongs to the first class\n",
    "# HINT use relplot, and the 'hue' and 'size' attributes\n",
    "sns.set_theme(font_scale=2)\n",
    "sns.relplot(x=my_features[0], y=my_features[1], hue='class', data=df, size=y_pred_LR_p[:,0], height=7) # XXX\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "df2eb749",
   "metadata": {},
   "source": [
    "What do you observe about probabilities with respect to sample position in feature space? \n",
    "\n",
    "Repeat the analysis with (ORC2+DOCK4, ORC2+COIL)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3eefd16d",
   "metadata": {},
   "source": [
    "Now, remember that our Bayes decision rule says we should decide for the class that has the maximum posterior probability. The hyperplane where these probabilities are equal is called the **decision boundary**. This is fairly easy to visualise in 1D, 2D, or 3D, and more difficult thereafter.\n",
    "\n",
    "So let's take advantage of our small feature space!\n",
    "\n",
    "We'll take inspiration (aka *feel free to copy-paste as long as you read the doc of the functions involved*) from this [scikit-learn example](https://scikit-learn.org/stable/auto_examples/ensemble/plot_voting_decision_regions.html). The principle is simple:\n",
    "1. Generate a large number of synthetic samples, with values interpolating between the smallest values and the largest values observed in real data.\n",
    "2. Compute (hard) predictions for all these synthetic points\n",
    "3. Display a contour plot of the predictions "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4d417035",
   "metadata": {},
   "outputs": [],
   "source": [
    "\n",
    "# Generate synthetic values between max and min of each gene's expression levels (np.meshgrid()).\n",
    "# Depending on how you do it, it may make your life easier to transform the dataframe back into a numpy array before finding the range of data.\n",
    "\n",
    "Xa=X.to_numpy()\n",
    "x_min, x_max = Xa[:, 0].min() - 1, Xa[:, 0].max() + 1\n",
    "y_min, y_max = Xa[:, 1].min() - 1, Xa[:, 1].max() + 1\n",
    "xx, yy = np.meshgrid(np.arange(x_min, x_max, 0.01),\n",
    "                     np.arange(y_min, y_max, 0.01))\n",
    "\n",
    "# reshape grid of synthetic values into a Nsyntheticpoints x 2 array you can use with the predict() function of your classifier\n",
    "new_points=np.vstack([xx.ravel(),yy.ravel()]).T\n",
    "\n",
    "# WRITE code to predict the class of all your synthetic data points\n",
    "new_preds=clf_LR.predict(new_points) # XXX\n",
    "\n",
    "# WRITE code to reshape your predictions so that they have the exact same shape as the synthetic grid values (xx).\n",
    "# This is needed for plt.contour().\n",
    "new_preds_reshaped=new_preds.reshape(xx.shape) # XXX\n",
    " \n",
    "    \n",
    "# Plot all synthetic predictions just to check.\n",
    "#plt.figure()\n",
    "#plt.imshow(new_preds_reshaped)\n",
    "\n",
    "# plot the decision boundary\n",
    "# your grid points xx and yy provide x and y coordinates, and your predictions provide 'elevation'.\n",
    "#plt.figure()\n",
    "#plt.contour(xx,yy,new_preds_reshaped) # XXX\n",
    "\n",
    "# plot data points + decision boundary\n",
    "plt.figure()\n",
    "plt.scatter(Xa[:,0],Xa[:,1],c=y, cmap='Spectral')\n",
    "plt.contour(xx,yy,new_preds_reshaped)\n",
    "\n",
    "# BONUS write a function that will plot decision boundary for you. Save it in a .py file. Now you can import it in this and other notebooks."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f3ff61b6",
   "metadata": {},
   "source": [
    "Does the decision boundary fall roughly where you expected from looking at the probabilities ?\n",
    "\n",
    "Experiment with the other 2 feature sets. Don't forget to re-train your classifier!"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "df839e19",
   "metadata": {},
   "source": [
    "## Hard predictions\n",
    "\n",
    "Now, let's look at 'hard' predictions, that is where the classifier will give you a class label (0 or 1) directly."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "35b319ed",
   "metadata": {},
   "outputs": [],
   "source": [
    "# WRITE sklearn code to give you hard predictions\n",
    "\n",
    "y_pred_LR=clf_LR.predict(X)\n",
    "\n",
    "# WRITE seaborn code to swarmplot hard predictions (x axis) vs soft predictions (y axis)\n",
    "# Recycle your code from the \"soft predictions\" section above. label your axes.\n",
    "\n",
    "plt.figure()\n",
    "sns.set_theme(font_scale=2)\n",
    "sns.swarmplot(x=y_pred_LR, y=y_pred_LR_p[:,0], hue='class', data=df, size=5) # XXX\n",
    "plt.xlabel('hard decision'); plt.ylabel('soft decision') # XXX\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "df6632cb",
   "metadata": {},
   "source": [
    "Contrast the distribution of probabilities with respect to the ground truth labels to the distribution of probabilities with respect to the predicted class labels. Do some sample switch side? Why?\n",
    "\n",
    "To make this more obvious try with the not-so-discriminative genes.\n",
    "\n",
    "Now let's see how hard predictions relate to the decision threshold, and how this impacts accuracy.\n",
    "\n",
    "Accuracy is simply the *proportion of samples that are classified correctly*, or the averaged 0-1 loss over your predictions."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fc186a85-a88a-4c89-aa5b-8d86e05abe1c",
   "metadata": {},
   "outputs": [],
   "source": [
    "%whos"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fea21da2",
   "metadata": {},
   "outputs": [],
   "source": [
    "from scipy.stats.contingency import crosstab\n",
    "\n",
    "my_thresh = 0.5 # Decision threshold\n",
    "\n",
    "# WRITE code to compute hard predictions yourself from the probabilistic predictions and the threshold my_thresh\n",
    "# HINT remember that y_pred_LR_p is nSamples x 2, so use the second (or first) column only in your comparison\n",
    "# HINT cast the vector to np.int32\n",
    "# HINT your y_pred_LR_p_thresh should be shape (106,) and dtype int32\n",
    "\n",
    "y_pred_LR_p_thresh=np.int32(y_pred_LR_p[:,1] > my_thresh) # XXX\n",
    "\n",
    "# WRITE code to compare your manually-thresholded prediction to the ones from scikit-learn (y_pred_LR).\n",
    "# This can be as basic as just printing the two and eyeballing the result, or you can use \n",
    "# crosstab from scipy.stats.contingency to compute a 2x2 contingency table showing agreements and disagreements\n",
    "# between your hand-thresholded hard decision vector and the original decision vector from sklearn.\n",
    "# HINT make sure you know which class is class 1 and which is class 0 (so, above or below threhsold).\n",
    "# If your predictions are all wrong then probably you mixed the two.\n",
    "\n",
    "(_, _), contingency_table = crosstab(y_pred_LR_p_thresh,y_pred_LR) # XXX\n",
    "print(contingency_table) # XXX\n",
    "\n",
    "# WRITE code to compute and print prediction accuracy from your y_pred_LR_p_thresh. Format the string nicely!\n",
    "# HINT you need to compare y_pred_LR_p_thresh to y\n",
    "# HINT first compute the number of correct predictions, store as n_correct. Then compute n_total. \n",
    "\n",
    "n_correct=np.sum(y_pred_LR_p_thresh==y) # XXX\n",
    "n_total=len(y_pred_LR_p_thresh)         # XXX\n",
    "print(f\"{n_correct}/{n_total} correctly classified. Accuracy {n_correct/n_total*100:2.2f}%.\") # XXX\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "18068fca",
   "metadata": {},
   "source": [
    "Now modify the threshold slightly (look at actual vakues of `y_pred_LR_p` to pick a threshold that is not too extreme), and\n",
    "* look at agreement between hard decisions from your own threshold and hard decisions from the default thresholding (in y_pred_LR)\n",
    "* recompute the accuracy. As always, remember that we are testing on the same dataset we trained on, so this accuracy will be biased high.\n",
    "\n",
    "\n",
    "How do the agreement with the default thresholding results and the accuracy change with respect to the threshold?\n",
    "\n",
    "Keep this effect in mind for when we'll talk about model evaluation, in particular about the sensitivity/specificity tradeoff."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5b19e742",
   "metadata": {},
   "source": [
    "---\n",
    "# High-dimensional case\n",
    "\n",
    "Here, we will explore the behaviour of logistic regression for classification in high-dimensional regimes, and the impact of regularization.\n",
    "\n",
    "Note that not all solvers can minimize all loss functions, depending on the regularization you use. Look at the doc for the `solver` argument of the `LogisticRegression` class.\n",
    "\n",
    "## Data preparation\n",
    "\n",
    "As in the linear regression case, we'll pick 300 gene as predictors."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "12aaa633",
   "metadata": {},
   "outputs": [],
   "source": [
    "G=300                                                             # pick 300 genes\n",
    "predictors_list=list(set(df.columns.values)-set(['class']))       # make a list of potential predictors, excluding class label\n",
    "\n",
    "X=df.loc[:,predictors_list[0:G]]\n",
    "y=(df['class'] == 'CX').astype('int32') # scikit-learn can deal with Pandas Series objects. For other packages you may add .to_numpy() if needed"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b490ca4b",
   "metadata": {},
   "source": [
    "## $\\ell_1$ \n",
    "\n",
    "Now, experiment with the $\\ell_1$ penalty. We'll use the same type of analyses as we did previously in the linear regression case, so you can reuse some code:\n",
    "- Effects of regularization on sparsity and coefficient distribution\n",
    "- Effects of regularization on accuracy (always keeping in mind we are over-optimistic since we are not evaluating on a separate dataset)\n",
    "\n",
    "Practical points: use solver `liblinear`. Be mindful that regularization parameters are set with the `C` argument, which is $\\frac{1}{\\lambda}$. So lower C = higher regularization."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5515b7d4",
   "metadata": {},
   "outputs": [],
   "source": [
    "# WRITE sklearn code to instantiate class LogisticRegression with the proper solver, l1 penalty, C=10, and random_state=42\n",
    "\n",
    "my_C=10\n",
    "\n",
    "clf_LR_L1=LogisticRegression(solver='liblinear', penalty='l1', C=my_C, random_state=42) # XXX\n",
    "\n",
    "# fit model on data X to predict target y\n",
    "clf_LR_L1.fit(X,y)\n",
    "\n",
    "# count the number of non-zero coefficients in the model.\n",
    "print(f\"LR+L1 NNZ {np.sum(clf_LR_L1.coef_ != 0 )}\")\n",
    "\n",
    "# predict (both soft and hard predictions). \n",
    "y_pred_LR_L1=clf_LR_L1.predict(X)\n",
    "y_pred_LR_L1_p=clf_LR_L1.predict_proba(X)\n",
    "\n",
    "# Compute accuracy\n",
    "n_correct=np.sum(y_pred_LR_L1==y)\n",
    "n_total=len(y_pred_LR_L1)        \n",
    "print(f\"{n_correct}/{n_total} correctly classified. Accuracy {n_correct/n_total*100:2.2f}%.\") # XXX\n",
    "\n",
    "# show distributions of class probabilities for each ground truth class\n",
    "sns.swarmplot(x=y,y=y_pred_LR_L1_p[:,0], hue=df['class'], size=4, alpha=0.8)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f3ea7afe",
   "metadata": {},
   "source": [
    "With C=10, you should have 22 non-zero coefficients, and an accuracy of 100%.\n",
    "\n",
    "Repeat this analysis with $C \\in \\{10,1,0.1,0.01,0.001\\}$.\n",
    "\n",
    "What happens to the sparsity of the solution at very high, respectively very low C values?\n",
    "\n",
    "What happens to the accuracy ? Do you trust results with $C=10$? Why or why not?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d4d83840",
   "metadata": {},
   "source": [
    "## $\\ell_2$\n",
    "\n",
    "Time for the $\\ell_2$ penalty.\n",
    "\n",
    "Here, use `lbfgs` as a solver. This is an implementation of the [Limited-memory Broyden-Fletcher-Goldfarb-Shanno algorithm](https://en.wikipedia.org/wiki/Limited-memory_BFGS)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5f4a3900",
   "metadata": {},
   "outputs": [],
   "source": [
    "# WRITE code suitable for L2-penalized logistic regression. Adapt your code from above.\n",
    "# Start with C=1, random_state=42, penalty='l2', solver='lbfgs'. You may need to increase the number of max. iterations.\n",
    "my_C=1\n",
    "\n",
    "clf_LR_L2=LogisticRegression(solver='lbfgs', penalty='l2', C=my_C, random_state=42, max_iter=1000) # XXX\n",
    "\n",
    "# fit model on data X to predict target y\n",
    "clf_LR_L2.fit(X,y)\n",
    "\n",
    "# Show the norm of the weight vector.\n",
    "print(f\"LR+L2 w norm {np.linalg.norm(clf_LR_L2.coef_):0.2f}\")\n",
    "\n",
    "# predict (both soft and hard predictions).\n",
    "y_pred_LR_L2=clf_LR_L2.predict(X)\n",
    "y_pred_LR_L2_p=clf_LR_L2.predict_proba(X)\n",
    "\n",
    "# Compute accuracy\n",
    "n_correct=np.sum(y_pred_LR_L2==y)\n",
    "n_total=len(y_pred_LR_L2)        \n",
    "print(f\"{n_correct}/{n_total} correctly classified. Accuracy {n_correct/n_total*100:2.2f}%.\")\n",
    "\n",
    "# show distributions of class probabilities for each ground truth class\n",
    "sns.swarmplot(x=y,y=y_pred_LR_L2_p[:,0], hue=df['class'], size=4, alpha=0.8)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b149f7fe",
   "metadata": {},
   "source": [
    "What error do you get?\n",
    "\n",
    "Do you understand the reason for two suggestions?\n",
    "\n",
    "If not have a very quick look at the description of L-BFGS. Similar to Newton's method, see how the function gradient will play a key role? What happens to the Hessian and its inverse if dimensions have very different magnitudes?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3c8f504d",
   "metadata": {},
   "outputs": [],
   "source": [
    "# WRITE pandas / numpy code to print out variance of each feature in increasing order\n",
    "\n",
    "print(-np.sort(-np.var(X, axis=1), axis=0)) # XXX"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9cb418f0",
   "metadata": {},
   "source": [
    "Not too dramatic here, as the features are around the same order of magnitude - but this is always worth a check. In any case, let's increase the number of iterations to 300 and try again."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5e86e049",
   "metadata": {},
   "outputs": [],
   "source": [
    "# WRITE code to replicate the analysis you did for L1 regularization, but with max_iter=300 in your LogisticRegression instantiation"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4aeed80a",
   "metadata": {},
   "source": [
    "Repeat your analysis with the same $C$ grid as for L1, and answer the same questions."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f191f729",
   "metadata": {},
   "source": [
    "## Elastic net\n",
    "\n",
    "Finally, we'll experiment with elastic net regularization. This is often a strong baseline, both for regression and classification choice, and applies to a wide variety of problems with biomedical data. \n",
    "\n",
    "\n",
    "This time, use `saga` as a solver."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f2b46d75",
   "metadata": {},
   "outputs": [],
   "source": [
    "# WRITE sklearn code to use LogisticRegression with the proper solver, 3000 iterations. don't forget the l1_ratio parameter\n",
    "# Start with C=10, l1_ratio=0.5\n",
    "\n",
    "# WRITE code to examine the usual quantities (probabilities, coefficients, accuracy...)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6a35e102",
   "metadata": {},
   "source": [
    "Again, repeat the analysis with hyperparameters $C \\in \\{10,1,0.1,0.01,0.001\\}$, and $\\mathrm{l1\\_ratio} \\in \\{1, 0.8, 0.5, 0.2, 0\\}$.\n",
    "\n",
    "Do coefficients behave as you expected? \n",
    "\n",
    "Does this depend on feature space dimensionality? Add more genes and find out...\n",
    "\n",
    "When do you see similarities with $\\ell_1$ penalty? With $\\ell_2$ penalty?\n",
    "\n",
    "As you can imagine, we need a more principled approach to setting hyperparameters. We will cover this in week 4.\n",
    "\n",
    "Now you have encountered firsthand many practical issues in regularized regression and classification. Congratulations on finishing this notebook!"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "dbb3450c",
   "metadata": {},
   "source": [
    "---\n",
    "# BONUS\n",
    "\n",
    "## What is the impact of regularization on correlated features?\n",
    "\n",
    "Try computing correlations between features (`np.corrcoef()`). Identify the most correlated features. Keep an eye out for them as you fit models with $\\ell_1$ and $\\ell_2$ regularization. Which 'share' (split) weights? Which get a zero weight? \n",
    "\n",
    "## Are the principles we learned here widely applicable?\n",
    "\n",
    "Since you have another dataset ready to go, go ahead and try some of the experiments in this notebook on the Fisher Iris dataset.\n",
    "\n",
    "## Do soft predictions really correspond to probabilities?\n",
    "\n",
    "If you want to dig deeper into how much soft outputs of various classifier correspond to probabilities, you can look at this nice comparison of [classifier calibration](https://scikit-learn.org/stable/auto_examples/calibration/plot_compare_calibration.html).\n",
    "\n",
    "## Is there an easier way to share data between notebooks?\n",
    "\n",
    "You can share objects such as dataframes (but not scikit-learn models) between notebooks by storing and retrieveing them using the `%store` magic. Check out the doc!\n",
    "\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.19"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
