# Introduction

This repo contains all the code snippets and code skeletons needed for the [hands-on introduction to machine learning for biomedical data](https://applicationspub.unil.ch/interpub/noauth/php/Ud/ficheCours.php?v_enstyid=79924&v_ueid=299&v_langue=8) doctoral school course at the University of Lausanne, taught by Jonas Richiardi.

Before the start of the course, please test that you can access the course [Moodle](https://moodle.unil.ch/course/view.php?id=17142) as well as log into [Nuvolos](https://nuvolos.cloud), using institutional login with your [Switch edu-ID](https://www.switch.ch/edu-id/).

Thanks to the students of the course for feedback helping me improve the code, and to Joan Rué Queralt for beta-testing all notebooks and providing experimental widget code.