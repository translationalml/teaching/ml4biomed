{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "5528db32",
   "metadata": {},
   "source": [
    "Fitting and regularizing linear models for regression\n",
    "=====================================================\n",
    "\n",
    "> All models are wrong, but some are useful - G. Box, 1976\n",
    "\n",
    "\n",
    "In this notebook you will experiment with linear models for **regression**. You will explore relationships\n",
    "between the dimensionality of the data (how many dimensions you have in your feature space with respect\n",
    "to the number of samples you have), and regularization.\n",
    "\n",
    "Given gene expression data for a tissue sample, our goal will be to predict whether the tissue sample came from the cortex of the cerebellum.\n",
    "\n",
    "After completing this notebook, you should be able to\n",
    "\n",
    "* Train a linear regression using least-squares fit through matrix inversion\n",
    "* Inspect and discuss model parameters\n",
    "* Differentiate between no regularization, l1 regularisation, and l2 regularization\n",
    "\n",
    "\n",
    "You need to **complete and run** the code blocks that contain a WRITE comment, and if you have time left you can complete the BONUS parts."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3de439de",
   "metadata": {},
   "source": [
    "---\n",
    "# Least squares fitting for linear regression\n",
    "\n",
    "## In-house implementation\n",
    "\n",
    "We start with simple multivariate regression. As you know you can use the normal equation (3.6 in ESL2) to perform an ordinary least-squares (OLS) fit to your data:\n",
    "\n",
    "$$\\hat{\\boldsymbol{\\beta}} = (\\mathbf{X}^T \\mathbf{X})^{-1} \\mathbf{X}^T \\mathbf{y}$$\n",
    "\n",
    "This estimator minimizes the residual sum of squares (RSS). Here, we'll use a somewhat contrived example: given the expression levels of two proteins (GABRA4 and DOCK4), can we predict the expression level of another (MOK)?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c3b538e7",
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "import numpy as np\n",
    "import seaborn as sns\n",
    "\n",
    "# WRITE code to load the pickled pandas df you prepared in the previous notebook.\n",
    "df=XXX\n",
    "\n",
    "N=df.shape[0]                                 # number of samples\n",
    "X=df[['GABRA4','DOCK4']]                      # focus on just two genes for now\n",
    "Xb=np.concatenate((np.ones((N,1)),X),axis=1)  # let's add an intercept to the design matrix\n",
    "y=df['MOK']\n",
    "\n",
    "# WRITE numpy code to estimate beta hat using Xb and y\n",
    "\n",
    "bhat=XXX\n",
    "\n",
    "# CHECK coefficients - array([-1.04980149,  0.18195303,  0.44531464])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "744a03b4",
   "metadata": {},
   "source": [
    "If you got the right result, well done. Please note however that this is not the way things are typically implemented, especially in high-dimensional cases ($N<D$), in particular due to concerns about matrix invertibility. Solid implementations typically use a matrix factorization step instead of directly inverting the matrix. We will examine these effects later.\n",
    "\n",
    "Now that we have a model, we can try to predict a MOK value from GABRA4 and DOCK3 values. In math, we're looking at equation 3.7 of \\[ESL2\\]:\n",
    "\n",
    "$$\\hat{y} = \\mathbf{X}\\hat{\\boldsymbol{\\beta}} $$\n",
    "\n",
    "Here we're doing in-sample validation (using same data for testing and training), so that doesn't tell us much about how this model would generalise to unseen data. Keep this in mind throughout, and we'll come back to this issue in week 3."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4085e819",
   "metadata": {},
   "outputs": [],
   "source": [
    "Xbnew=Xb[0,:] # let's pick the first sample. \n",
    "\n",
    "# WRITE code to predict MOK4 expression (yhat) from GABRA4 and DOCK4 values in this sample (Xbnew) and your model (bhat)\n",
    "yhat=XXX\n",
    "\n",
    "# WRITE code to compare to the real value (stored in y).\n",
    "\n",
    "Xbnew=Xb[10,:] # let's pick the 11th sample. \n",
    "\n",
    "# WRITE code to predict again, and compare to real value.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ef0c4e37",
   "metadata": {},
   "source": [
    "As you can see error varies depending on samples (as expected!). Let's look at our predictions and ground truth for all the dataset at once"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "31a8c5de",
   "metadata": {},
   "outputs": [],
   "source": [
    "sns.set_theme(font_scale=1.5) # larger fonts\n",
    "y_pred=Xb.dot(bhat)           # predict from all samples at once\n",
    "my_fg=sns.relplot(x=y,y=y_pred, hue=df['class'])\n",
    "my_fg.set_axis_labels('MOK','Predicted MOK')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "37868f5a",
   "metadata": {},
   "source": [
    "This looks OK-ish for cortex, not so good for cerebellum. Again, remember this is showing predictions on the same dataset we used for training, so we cannot claim that it will generalise to unseen data.\n",
    "\n",
    "Now, let's quantify the prediction error. Here we'll use the root mean square error (RMSE), which is simply the square root of the average of the residual sum of squares (RSS) you just minimized above."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8247551d",
   "metadata": {},
   "outputs": [],
   "source": [
    "# WRITE a function to compute and return the root mean square error (RMSE). It should take as inputs a 1D numpy array\n",
    "# of ground truth and a 1D numpy array of predictions\n",
    "\n",
    "def my_RMSE(y: np.array, y_pred: np.array) -> np.float64:\n",
    "    \"\"\"\n",
    "    Compute root mean square error for an array of predictions\n",
    "    Args:\n",
    "        y: the ground truth (prediction target) as floating point values\n",
    "        y_pred: the model predictions\n",
    "    Returns:\n",
    "        root mean square error\n",
    "    \"\"\"\n",
    "    rmse_val=XXX YOUR CODE HERE\n",
    "    return rmse_val\n",
    "\n",
    "# WRITE code to call your function and compute RMSE on your predictions.\n",
    "\n",
    "# BONUS compute RMSE separately for cortex and cerebellum samples"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1af583d1",
   "metadata": {},
   "source": [
    "## Scikit-learn implementation\n",
    "\n",
    "Now that you know how to do it yourself, let's start looking at scikit-learn. This package contains most of the functionality you will need to apply (classical) machine learning to your own work on a daily basis.\n",
    "\n",
    "Here, we will focus on two sub-packages: \n",
    "- `sklearn.linear_model`\n",
    "- `sklearn.metrics`\n",
    "\n",
    "### Model fitting with scikit-learn"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d1d2048d",
   "metadata": {},
   "outputs": [],
   "source": [
    "# WRITE sklearn code to import the proper class for linear regression and MSE computation\n",
    "\n",
    "# WRITE sklearn code to fit a linear model to predict MOK values from GABRA4 and DOCK4 values\n",
    "\n",
    "# WRITE code to print out the intercept and coefficients of the model. \n",
    "\n",
    "# CHECK it matches your own calculation"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e909b993",
   "metadata": {},
   "source": [
    "Once you get the same result as with your own implementation (possibly with a few minor differences), let's try prediction.\n",
    "\n",
    "### Prediction with scikit-learn\n",
    "\n",
    "Here we want to apply the model you've just trained. The API in scikit-learn in consistent, in that you always call `predict()` no matter what the underlying model is doing. Again, we predict on the same data we used for training, so we cannot claim that our model generalises."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1d5a8172",
   "metadata": {},
   "outputs": [],
   "source": [
    "# WRITE sklearn code to predict MOK values from GABRA4 and DOCK4 values, using your trained model\n",
    "\n",
    "# WRITE sklearn code to compute RMSE on your predictions (use sklearn functions, not your own)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6eedfd4d",
   "metadata": {},
   "source": [
    "Do you get the same performance as with your own code? Why or why not?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8f09a2f0",
   "metadata": {},
   "outputs": [],
   "source": [
    "# BONUS write code to split the data, using 80% of the data for fitting the model, and 20% for prediction.\n",
    "# Depending on how you do it there could be some ordering in the samples, but we'll see how to fix that next week.\n",
    "# Compare the RMSE you got previously (on the same samples) with the one you are getting now."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a89f8f5a",
   "metadata": {},
   "source": [
    "----\n",
    "# Dimensionality issues and regularization\n",
    "\n",
    "So far we had a low-dimensional problem, since we had $D=2$ genes and $N=106$ samples. In high dimensional problems (where $N<D$), things become more tricky. First, if there are more features, some might be correlated, and corresponding coefficients (betas) will be very sensitive to small variations in the data. Importantly, in this case the $\\mathbf{X}^T \\mathbf{X}$ matrix that you need to invert can become ill-conditioned and possibly singular.\n",
    "\n",
    "In biomedical data, it is very common to have high-dimensional problems with correlated predictors. This happens for instance in medical imaging (tens of thousands of voxels with strong spatial correlations within in a single image), genetics (millions of SNPs in linkage disequilibrium), etc. Thus, regularization is an essential part of your toolkit.\n",
    "\n",
    "In addition, certain types of regularization promote *sparsity*. That is, they enable models to form good predictions with as few features as possible. This is often important for interpretability in biomedical data.\n",
    "\n",
    "## L2 regularization\n",
    "\n",
    "So, as explained in \\[ESL2\\], a first recourse is to use penalized regression using different penalties (regularization terms). We'll start with $\\ell_2$-regularization, a.k.a the ridge penalty. As you have seen during preparation, the OLS estimator is modified as follows:\n",
    "\n",
    "$$\\hat{\\beta}_{ridge} = (\\mathbf{X}^T \\mathbf{X} + \\lambda \\mathbf{I})^{-1} \\mathbf{X}^T \\mathbf{y}$$\n",
    "\n",
    "This seemingly simple change has wide-ranging consequences, as we will see. Indeed, you should be suspicious of any high-dimensional fitting you do  (or read about) if there is no regularization in place. \n",
    "\n",
    "If you are interested in digging deeper into connections with other branches of engineering (e.g. image reconstruction), note that $\\ell_2$-regularization is a special case of Tikhonov regularization where the Tikhonov matrix $\\mathbf{\\Gamma} = \\lambda \\mathbf{I}$.\n",
    "\n",
    "## In-house implementation\n",
    "\n",
    "In our previous example, we predicted MOK expression levels in the brain using only 2 genes. Nothing guarantees that GABRA4 and DOCK4 are particularly related to MOK, and biological pathways database are notoriously patchy and prone to change. So let's use 300 genes - after all, more predictors should results in better predictions, no? "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d463fa79",
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.metrics import mean_squared_error\n",
    "from sklearn.linear_model import LinearRegression, Ridge\n",
    "\n",
    "G=300                                                             # pick 300 genes\n",
    "predictors_list=list(set(df.columns.values)-set(['class','MOK'])) # make a list of potential predictors, excluding our gene target and class label\n",
    "\n",
    "X=df.loc[:,predictors_list[0:G]]\n",
    "Xb=np.concatenate((np.ones((N,1)),X),axis=1)\n",
    "\n",
    "# WRITE code to compute the X'X matrix with NO regularization\n",
    "\n",
    "S_HD=XXX\n",
    "\n",
    "# WRITE code to compute and show the rank, condition number, and determinant of the S_HD matrix\n",
    "\n",
    "# WRITE code to compute the OLS estimator for beta hat (NO regularization), using the normal equation, then print out the first three components\n",
    "\n",
    "# WRITE code to form predictions (y_pred_HD), sklearn code to compute RMSE, and Seaborn code to plot ground truth MOK vs predicted MOK"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "454c84b4",
   "metadata": {},
   "source": [
    "How good are your predictions?\n",
    "\n",
    "We can fix this! Let's redo this with a ridge penalty.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8c1ce0dc",
   "metadata": {},
   "outputs": [],
   "source": [
    "lamda=100 # ! not a spelling mistake - Python reserves the word lambda so be careful\n",
    "\n",
    "# WRITE code to compute the X'X matrix WITH ridge regularization\n",
    "\n",
    "S_HD_reg=XXX\n",
    "\n",
    "\n",
    "# WRITE code to compute and show the rank, condition number, and determinant of the S_HD_reg matrix\n",
    "\n",
    "# WRITE code to compute the ridge estimator for beta hat (with regularization), then print out the first three components\n",
    "\n",
    "# WRITE code to compute predictions (y_pred_HD_reg), sklearn code to compute RMSE, and Seaborn code to plot ground truth MOK vs predicted MOK\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f8744cb1",
   "metadata": {},
   "source": [
    "Keep in mind that *typically you would rescale or standardize your inputs prior to ridge regression*. Here we are skipping this step because a) all genes are roughly in the same range (at most 1 order of magnitude apart) and b) preprocessing as a topic deserves full discussion which we will do next week!"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d000742e",
   "metadata": {},
   "source": [
    "### Effects of regularization strength and the regularization path"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a8d00ed1",
   "metadata": {},
   "source": [
    "So far we've picked a single lambda value. Remember it sets the tradeoff between data fit and shrinking the vector of model parameters, so how do we know if it's a good choice?\n",
    "\n",
    "And more philosophically, what constitutes a *good choice*? Empirically, you want\n",
    "- Enough regularization that you can invert your moment matrix\n",
    "- Not so much regularization that you just shrink everything to 0 and fail to fit the data.\n",
    "\n",
    "Now, let's experiment with various $\\lambda$ values."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "15fd4f56",
   "metadata": {},
   "outputs": [],
   "source": [
    "# WRITE Numpy code to generate 100 logarithmically-spaced lambda values between 0 and 1000. Store in my_grid.\n",
    "# You may need to add the 0 separately.\n",
    "\n",
    "my_grid=XXX\n",
    "\n",
    "# WRITE a Python loop to quickly experiment with these values (fit your model with various regularization strengths),\n",
    "# storing resulting RMSE values in a numpy array.\n",
    "# Pre-allocate and index, or use append.\n",
    "\n",
    "# WRITE Seaborn code to plot lambda vs RMSE (relplot). Use log scale for one axis (plt.xscale('log')).\n",
    "# If you have issues with plotting log(0) you can add a small constant, say 10^-1 just for plotting purposes"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "29db86ed",
   "metadata": {},
   "source": [
    "What you just did is a very simple grid search! It is a simple and useful way of selecting hyperparameters, and we'll come back to this in week 4 to explore just how much impact it has on performance.\n",
    "\n",
    "What happened to RMSE as you varied $\\lambda$? How do you explain this?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "80073f9d",
   "metadata": {},
   "source": [
    "Now, let's examine how model coefficients change with various levels of regularization. This is called the **regularization path** and is very helpful in understanding when various coefficients enter the model, and appreciate their variance with various levels of $\\lambda$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d8a993a8",
   "metadata": {},
   "outputs": [],
   "source": [
    "# WRITE a loop to fit your model with the various regularization strengths. for each value of lambda, store \n",
    "# the coefficients for the first 10 genes (excluding the intercept) into a 100 x 10 array.\n",
    "# Either Pre-allocate and index into your array, or use append / stack.\n",
    "\n",
    "# WRITE Seaborn code to plot the 10 coefficients vs lambda. Use log scale for one axis. Alternatively use matplotlib.\n",
    "\n",
    "# HINT This is maybe easier if you create a proper pandas dataframe, with 10 columns (you have all names in predictors_list), and \n",
    "# index given by my_grid.\n",
    "#\n",
    "# There are numerous ways of creating the df itself. I recommend using np.hstack. Remember last week's trick \n",
    "# to turn a 1D array (100,) into a proper \"2D\" vector (100,1) using reshape?. Might be useful.\n",
    "#\n",
    "# Also - you can set the index on a pandas dataframe with my_df.set_index('my_var_name', inplace=True)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4b597b4d",
   "metadata": {},
   "source": [
    "Does the regularization path behave as you expected?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b2f83cd0",
   "metadata": {},
   "source": [
    "## Scikit-learn implementation\n",
    "\n",
    "I should point out that the naïve in-house implementation we coded above is not particularly robust, and I do **not recommend using it for real high-dimensional problems**. Instead, the scikit-learn implementation is solid and offers multiple solvers.\n",
    "\n",
    "As before, the model we need, `Ridge`, is in `sklearn.linear_model`.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "018a0aa0",
   "metadata": {},
   "outputs": [],
   "source": [
    "# WRITE sklearn code to create a Ridge classifier, then train it to predict MOK expression from the same 300 genes, using lambda = 10 (called alpha\n",
    "# in the scikit-learn implementation)\n",
    "\n",
    "# WRITE code to compute ridged predictions (y_pred_HD_SKL_reg), sklearn code to compute RMSE, and Seaborn code to plot ground truth MOK vs predicted MOK \n",
    "\n",
    "# WRITE Seaborn code to do a jointplot of your Linear model coefficients versus the Ridge model coefficients. Try with various lambdas."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7d0bc313",
   "metadata": {},
   "source": [
    "Does the `alpha=0` version of the scikit-learn Ridge estimator yield solutions different from your own `lambda=0` version? What are possible explanations?\n",
    "\n",
    "How does the marginal distribution of coefficients in the Ridge model vary with different $\\lambda$?\n",
    "\n",
    "How does the joint distribution of (Linear,Ridge) coefficients vary with different $\\lambda$?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "875aacd9",
   "metadata": {},
   "source": [
    "## L1 regularization \n",
    "\n",
    "Also called Lasso in the statistics community, or basis pursuit in signal processing, the $\\ell_1$ penalty is sparsity-promoting. This means that depending on regularization strength, many parameters will be set to exactly 0.\n",
    "\n",
    "Note that the way \\[ESL2\\] (section 3.4.2) writes the Lasso penalty is in fact equivalent to adding a term $\\lambda ||\\boldsymbol{\\beta}||_1$ to the data fit term. Note also that there is no analytic solution as we had for the ridge penalty, so we must rely on solvers.\n",
    "\n",
    "In scikit-learn, the model we need, `Lasso`, is again in `sklearn.linear_model`.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6cddda08",
   "metadata": {},
   "outputs": [],
   "source": [
    "# WRITE sklearn code to fit a Lasso model predicting MOK expression from the 300 genes, using alpha=1e-2\n",
    "\n",
    "# WRITE sklearn code to compute Lasso predictions (y_pred_HD_SKL_L1), sklearn code to compute RMSE and R2,\n",
    "# and Seaborn code to plot ground truth MOK vs predicted MOK \n",
    "\n",
    "# WRITE seaborn code to plot a histogram of the model coefficients (50 bins).\n",
    "\n",
    "# WRITE a loop to explore the effect ot 50 different logarithmically-spaced lambda (alpha) values, from 1e-5 to 1.\n",
    "# each time, compute RMSE, r-squared, and the number of non-zero coefficients in the model (sparsity)\n",
    "\n",
    "# WRITE seaborn code to plot lambda vs RMSE, lambda vs r-squared, and lambda vs sparsity"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "db9bbc94",
   "metadata": {},
   "source": [
    "Contrast the shape of the distribution of coefficients with the ridge solutions.\n",
    "\n",
    "Did you get convergence problems ? \n",
    "\n",
    "What happens to RMSE and R2 when $\\lambda$ is varied?\n",
    "\n",
    "What happens to sparsity when $\\lambda$ is varied?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1b60bb97",
   "metadata": {},
   "source": [
    "## Elastic net regularization \n",
    "\n",
    "A final approach to regularization we will look at is the elastic net penalty, a linear combination of $\\ell_1$ an $\\ell_2$ penalties. This has a sparsity-promoting behaviour by forcing coefficients to zero, and a shrinkage behaviour by reducing magnitudes of coefficients corresponding to correlated features.\n",
    "\n",
    "Unsurprisingly, `ElasticNet` is in `sklearn.linear_model`. For R people, it's broadly equivalent to `glmnet`.\n",
    "\n",
    "`ElasticNet` is sligthly more finnicky to tune than the Ridge or Lasso models however - you need to tune both a general regularization parameter `alpha`, and another parameter `l1_ratio` which controls the linear combination, where 0 means pure $\\ell_2$ and 1 means pure $\\ell_1$.\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "77dfc4d4",
   "metadata": {},
   "outputs": [],
   "source": [
    "alpha=0.1    # try values between 1 and 1e-2\n",
    "l1_ratio=0.5 # try values between 1e-1 and 1\n",
    "\n",
    "# WRITE sklearn code to fit an elastic net predicting MOK expression from the 300 genes, using the parameters above\n",
    "\n",
    "# WRITE code as in L1 and ridge case, to predict values, compute RMSE and R2, plot prediction results, and coefficient histogram.\n",
    "\n",
    "# WRITE a double loop to explore the effects of varying alpha between 1 and 1e-2 (4 different values), and l1_ratio\n",
    "# between 1e-1 and 1 (4 different values). Each time record the two parameter values, RMSE, R2, and sparsity.\n",
    "\n",
    "# WRITE Seaborn code to explain your results, possibly using multi-plot grids\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f35af0b1",
   "metadata": {},
   "source": [
    "---\n",
    "# BONUS\n",
    "\n",
    "Only do this if you have time - complete the other notebooks first\n",
    "\n",
    "## Model checks \n",
    "\n",
    "- The MLE we are using is valid for Gaussian distributions. But our data may not be distributed according to our assumptions. Perform some checks on the residuals (= prediction errors, $\\hat{\\mathbf{y}}-\\mathbf{y}$) by plotting predictions vs residuals as a scatterplot. Do you see a trend? Is the variance of errors distributed evenly across the range? If not, our model may be mis-specified, maybe an OLS fit is not the way to go, and we should think about using proper distributional assumptions about our residuals...\n",
    "\n",
    "## Solvers\n",
    "- Experiment with the various solvers for Ridge, including the 'svd' and 'cholesky' solvers. What are the differences in terms of RMSE? In terms of predictions?\n",
    "\n",
    "## Model comparison\n",
    "- Think about a strategy to compare the three models fairly. What metrics would you use? What evaluation procedure? Would this tell you how well the models are likely to generalize?\n",
    "- Experiment with `sklearn.model_selection.train_test_split` to compare models\n",
    "\n",
    "## Effect of regularization on singular values of the moment matrix\n",
    "\n",
    "- What effect does adding $\\lambda\\mathbf{I}$ to the moment matrix have on the spectrum of the matrix? Try an [SVD](https://numpy.org/doc/stable/reference/generated/numpy.linalg.svd.html) with $\\lambda=0$ and a non-zero $\\lambda$."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.13"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
