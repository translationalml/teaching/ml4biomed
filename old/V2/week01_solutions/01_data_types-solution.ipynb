{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "3fa62b1e",
   "metadata": {},
   "source": [
    "Introduction to Python native data types and Numpy arrays\n",
    "=========================================================\n",
    "\n",
    "The goal of this notebook is to build your foundations with Python native data types and Numpy data types.\n",
    "\n",
    "After completing this notebook, you should be able to\n",
    "\n",
    "* Identify and select the appropriate data types for your data science work\n",
    "* Create and modify numerical arrays\n",
    "* Perform basic linear algebra and compute basic descriptive statistics\n",
    "\n",
    "You need to **complete and run** the code blocks that contain a WRITE comment, and if you have time left you can complete the BONUS parts."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d2ccf840",
   "metadata": {},
   "source": [
    "---\n",
    "# Standard data types\n",
    "\n",
    "If you've programmed in a high-level language before (C++, Java...), Python data types will be fairly familiar, but there are a few specificies, and results of numerical operations may surprise you. In particular, if you're used to statically typed languages like C++, you will see that Python gives you a lot of flexibility.\n",
    "\n",
    "## Numeric, boolean, and string types\n",
    "\n",
    "Let's experiment with some basic data types and operators.\n",
    "\n",
    "The main types you need are \n",
    "- integers (`int`)\n",
    "- floating-point numbers (`float`)\n",
    "- boolean values (`bool`), and\n",
    "- strings (`str`)\n",
    "\n",
    "Mathematical operators are what you expect: `+`, `-`, `/`, `*`.\n",
    "\n",
    "Shorthand assignment works as in C++: `*=` etc\n",
    "\n",
    "Modulo is `a%b` (e.g. `5%2` gives `1`) and powers are `a**b`.\n",
    "\n",
    "Boolean values are `True` and `False`.\n",
    "\n",
    "You can always check the type of your variable like this:\n",
    "\n",
    "```python\n",
    "foo=1.\n",
    "type(foo)\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8ae3f211",
   "metadata": {},
   "source": [
    "### Concatenating strings\n",
    "\n",
    "You will very often need to print out results interspersing text and dynamic numerical values. Let's say we have $N=42$ samples in your dataset. As a check it is always a good idea to print out the number of samples and their dimensions.\n",
    "\n",
    "It's also common to show results only up to some precision."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f0207a7b",
   "metadata": {},
   "outputs": [],
   "source": [
    "# RUN this\n",
    "N=42                  # we have 42 samples of different flower types...\n",
    "correct_classif_1=30  # ...out of which we classified 30 samples correctly using machine learning\n",
    "\n",
    "# WRITE some code using f-strings or the .format() function to output the string \"Loaded N samples\" where \"N\" is replaced by the value \n",
    "# assigned above. Change the values and run the cell to make sure it does what you want.\n",
    "print(f\"Loaded {N} samples\")\n",
    "\n",
    "# WRITE code that prints classification accuracy as \"Accuracy = 71.4 %\", using variables N and correct_classif_1, together with the proper\n",
    "# type specifier. Use f-strings or .format() as you prefer. Run cell to check.\n",
    "\n",
    "print(f\"Accuracy = {(correct_classif_1/N)*100:0.1f} % \")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f5665c96",
   "metadata": {},
   "source": [
    "### Comparison operators\n",
    "\n",
    "You will routinely need to check if a number is larger or different than another. Let's practice that.\n",
    "\n",
    "Say we have two 'flower classifiers', that is, algorithms that take as input measurements of the flowers, and output a prediction of which flower type it is. Further, they have different classification accuracies, and we want to compare them."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f303d5ee",
   "metadata": {},
   "outputs": [],
   "source": [
    "# RUN this\n",
    "classifier_name_1='SVM'   # our first classifier is a support vector machine (we'll work with those in week 3)\n",
    "classifier_name_2='RF'    # our second classifier is a random forest (we'll work with those in week 4)\n",
    "correct_classif_2=35      # this classifier is slightly more accurate - it predicts 5 more flowers correctly than classifier 1.\n",
    "acc_1=correct_classif_1/N # accuracy of our first classifier\n",
    "acc_2=correct_classif_2/N # accuracy of our second classifier\n",
    "\n",
    "# WRITE some code that checks\n",
    "# - if the accuracy of the first classifier is the same than that of the second classifier\n",
    "# - if it is higher than that of the second\n",
    "# - what is the type returned by this comparison\n",
    "\n",
    "c1_equals_c2=acc_1 == acc_2\n",
    "print(c1_equals_c2)\n",
    "\n",
    "c1_greater_than_c2=acc_1 > acc_2\n",
    "print(c1_greater_than_c2)\n",
    "\n",
    "print(type(c1_greater_than_c2))\n",
    "\n",
    "# WRITE some code that checks if the strings containing the two classifier names are the same length\n",
    "c_names_equal_length=len(classifier_name_1)==len(classifier_name_2)\n",
    "print(f\"strings are same length: {c_names_equal_length}\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f530b611",
   "metadata": {},
   "source": [
    "## Compound types \n",
    "\n",
    "In machine learning we're typically more interested in processing several numbers, not just scalars. Python offers *compound types* which allow you to group numbers into single objects.\n",
    "\n",
    "We are interested mostly in lists (which are *mutable*) and tuples (which are *immutable*).\n",
    "\n",
    "Botany time! Let's go out in the field, and measure petal lengths $x$ (the data, or *features*) for two iris flower species $y \\in \\{0,1\\}$ (the *labels*, or class labels), sampling 4 flowers of each species in total."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a5cdbda3",
   "metadata": {},
   "outputs": [],
   "source": [
    "# RUN this\n",
    "x_l=[1.4, 1.4, 1.3, 1.5, 4.7, 4.5, 4.9, 3.9] # petal length (in cm)\n",
    "y_l=[0, 0, 0, 0, 1, 1, 1] # species numerical code (0=Iris setosa, 1=Iris versicolor)\n",
    "\n",
    "# WRITE code to check and print how many samples you have, and how many labels you have.\n",
    "# WRITE code to check that you have as many labels as samples. If not, fix the problem by using append()."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4dd316cc",
   "metadata": {},
   "source": [
    "## Indexing and slicing in compound types\n",
    "\n",
    "As you've learned before class, you can access individual items in a compound type using bracket notation \\[ \\].\n",
    "\n",
    "Let's see how that goes.\n",
    "\n",
    "First, let's also record the geolocation of our sampling in a tuple. This is a reasonable data type for this type of coordinate-like data.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0e0e4b9f",
   "metadata": {},
   "outputs": [],
   "source": [
    "# RUN this\n",
    "coords=(28.523, 80.682) # LAT °N, LON °W"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bced7793",
   "metadata": {},
   "source": [
    "Looking at the Fisher iris data and comparing with the famous paper, we realise that the last petal length (of species 1) was entered incorrectly as 3.9. It should be 4.0. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7564a7e0",
   "metadata": {},
   "outputs": [],
   "source": [
    "# WRITE code to fix this error in x_l by assigning the correct value at the proper index. \n",
    "# WRITE code to do the same, but this time exploiting the fact that it's the last item in the list that was wrong, without\n",
    "# explicitly specifying position\n",
    "\n",
    "# check your result, at this point you should get\n",
    "# x_l==[1.4, 1.4, 1.3, 1.5, 4.7, 4.5, 4.9, 4.0]\n",
    "# y_l==[0, 0, 0, 0, 1, 1, 1, 1]\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "965b3733",
   "metadata": {},
   "source": [
    "Great. While we're at it we should fix the coordinates as well, the longitude is actually 80.683°W."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "53d32e04",
   "metadata": {},
   "outputs": [],
   "source": [
    "# WRITE code to correct the tuple.\n",
    "\n",
    "# Did it work? How can we fix the coordinates?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "99f0c150",
   "metadata": {},
   "outputs": [],
   "source": [
    "# BONUS read the doc on the zip() built-in function and make a list of tuples with (petal length, label). Keep this function in mind as it's often useful."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "271630f9",
   "metadata": {},
   "source": [
    "---\n",
    "# Numpy arrays\n",
    "\n",
    "Numpy is the workshorse in Python numerical computing. Amongst other goodies, it provides an array object that is used by virtually all data science packages, including pandas and scikit-learn. So let's spend some time getting to know arrays better!\n",
    "\n",
    "Note that numpy arrays contain only one type of data, which you can conveniently check with `my_array.dtype`.\n",
    "\n",
    "## Creating numpy arrays\n",
    "\n",
    "### Manual creation\n",
    "\n",
    "Now we have our flower data collected in lists `x_l` and `y_l` as above. Working with numpy arrays is generally preferable (why?), so let's create some arrays."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a6155682",
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np # this is how you import a package. You can give it a short name. np is customary for numpy.\n",
    "\n",
    "# WRITE code to create two numpy arrays, one with the features called x and one with the labels called y from the respective lists\n",
    "x=XXX # petal length\n",
    "y=XXX # species numerical code\n",
    "\n",
    "\n",
    "# BONUS can you specify the data type for each of these arrays? What would you choose for each?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "747816a2",
   "metadata": {},
   "source": [
    "Now again let's check that the dataset size is consistent with the number of labels you have! This is generally something you want to do as a matter of principle.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0a73a3aa",
   "metadata": {},
   "outputs": [],
   "source": [
    "# WRITE some code to print out the number of elements you have in each your data $x$ and labels $y$, without using the len() function.\n",
    "# Then remove the exception.\n",
    "raise NotImplementedError(\"Please complete the exercise\")\n",
    "\n",
    "# BONUS check automatically that they are consistent, print a message if not."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3f6bcfb9",
   "metadata": {},
   "source": [
    "### Built-in functions for array creation\n",
    "\n",
    "Four array types will come up again and again, which have dedicated functions to create them: empty arrays `np.empty()`, arrays of ones `np.ones()`, arrays of zeros `np.zeros()`, and arrays of numeric sequences `np.arange()`, `np.linspace()`.\n",
    "\n",
    "For example, if you suspect that typing class labels by hand is probably not doable beyond toy datasets, you are right!\n",
    "Let's remedy this."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "24e89113",
   "metadata": {},
   "outputs": [],
   "source": [
    "# WRITE code to create the same labels $y$ for your iris data as you had previously, but using built-in \n",
    "# functions for array creations (no manual typing of labels!), combined with array concatenation. As always check sizes.\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f7ea7027",
   "metadata": {},
   "source": [
    "## Indexing and slicing arrays\n",
    "\n",
    "Like lists, and following very similar logic, numpy arrays can be indexed and sliced. One extremely common operation is called *logical indexing* (also familiar to Matlab users), whereby you generate a vector of booleans to serve as indices to an array, and only elements marked True are retained.\n",
    "\n",
    "Let's try to use this to select only class 0 or class 1 samples.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8659f8cc",
   "metadata": {},
   "outputs": [],
   "source": [
    "# WRITE a code line that creates a numpy boolean array that has value True where the label array (y) has class 0 (Iris setosa), and False elsewhere\n",
    "my_idx=XXX\n",
    "\n",
    "# WRITE Now use this to index the x array so you can print only Iris setosa petal lengths\n",
    "\n",
    "# WRITE Rewrite this to avoid creating the my_idx array explicitely, instead directly indexing the x array\n",
    "\n",
    "# BONUS explore using np.nonzero to achieve the same end. What is the type of data in this array?"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ecc51fcf",
   "metadata": {},
   "source": [
    "## More array manipulation - reshaping and combining arrays\n",
    "\n",
    "Up to now we've been using univariate (1D) data. This is hardly ever the case in real applications, where typically one works with $D$-dimensional feature vectors, stacked into a $N \\times D$ feature matrix, where $N$ is the number of samples (cases, records). "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7f76a38e",
   "metadata": {},
   "source": [
    "So, let's get our flower specimens out once more and measure [sepals](https://en.wikipedia.org/wiki/Sepal#/media/File:Mature_flower_diagram.svg) in addition to petals. This will be a second dimension in our array."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "187dbce7",
   "metadata": {},
   "outputs": [],
   "source": [
    "sepal_data=np.array([5.1, 4.9, 4.7, 4.6, 7.0, 6.4, 6.9, 5.5]) # sepal length data for the same flower samples as before\n",
    "\n",
    "# WRITE code to add ('stack') the sepal length data to the array containing the petal length data"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "063474e5",
   "metadata": {},
   "source": [
    "Now to fix array dimensions (axes) properly, let's make sure we are explicit about which way our data is laid out. Remember we want an $N \\times D$ matrix. This convention will also make it easier to select and filter data when we use pandas dataframes (or R dataframes), where features are columns."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "885cdf35",
   "metadata": {},
   "outputs": [],
   "source": [
    "# WRITE code to transform x an 8x2 array while ensuring feature vectors are kept correctly (so a column vector becomes a row vector with the same components)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b1acd132",
   "metadata": {},
   "source": [
    "Now look at your array and ensure that the first flower has petal, sepal = `[1.4, 5.1]` and the last has petal, sepal = `[3.9, 5.5]`. If not, be careful! Read the doc for `numpy.reshape`, especially the `order` keyword. Then read the doc for `np.transpose`. [This post](https://lihan.me/2018/01/numpy-reshape-and-transpose/) should help clear the confusion."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e77d2cb7",
   "metadata": {},
   "outputs": [],
   "source": [
    "# BONUS add a new setosa after the 4th, and a new versicolor at the end of the x array. Also add class labels at the proper locations in y."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3af26cd5",
   "metadata": {},
   "source": [
    "## Basic mathematical operations and descriptive stats\n",
    "\n",
    "At this point our dataset is small but starting to look more realistic. We have two classes, 2 dimensions (features), and 8 samples. Let's start doing some computations and obtain some statistics.\n",
    "\n",
    "As a reminder, elementary operations `+`, `-`, `/`, `*` on arrays are elementwise. So the product is not the dot product but the Hadamard product. We'll get to linear algebra later.\n",
    "\n",
    "You have collected data, and now you want to send your results to the Journal of the Swiss Botanical Society, but they insist on using millimeters for all measurements.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5867ec88",
   "metadata": {},
   "outputs": [],
   "source": [
    "# WRITE code to convert your measurements to mm, assigning to the same array"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "151d74a4",
   "metadata": {},
   "source": [
    "We need to get some summary statistics to compare iris types. Fortunately numpy comes equipped with some functionality already."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "06c254aa",
   "metadata": {},
   "outputs": [],
   "source": [
    "# WRITE code to show how many distinct (unique) classes of flowers you have in your y array\n",
    "\n",
    "# WRITE code to compute, for each flower type separately, the mean and standard deviation of petal length and sepal length"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "94fb62ae",
   "metadata": {},
   "source": [
    "For the means: check that you get `[14. , 48.25]` mm for _iris setosa_, and `[45. , 64.5]` mm for _iris versicolor_ (values may differ if you completed the BONUS section above, since you would now have 5+5 iris flowers). If your values don't match, read the doc for `np.mean` and check your `axis` keyword. To debug, print only the measurements for setosa, and only the measurements for versicolor.\n",
    "\n",
    "Now we would also like to show the median and range of the data (min - max) for petal and sepal measurements, separately for each flower."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7d98768d",
   "metadata": {},
   "outputs": [],
   "source": [
    "# WRITE code that prints summary stats neatly, in the format \"median (min, max)\" for each iris type. You can assign temporary variables to\n",
    "# make your code cleaner. Use f-strings for neatness."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6e0b65b7",
   "metadata": {},
   "source": [
    "## Linear algebra\n",
    "\n",
    "To complete our intro to numpy, let's quickly review basic matrix operations you'll need for machine learning\n",
    "\n",
    "- Array to vector: `y.reshape(y.shape[0],1)`\n",
    "- Matrix transposition: `X.T`\n",
    "- Matrix inversion: `np.linalg.inv(X)`\n",
    "- Dot product (inner product): `np.dot(X,Y)` (use for 1D arrays (vectors), or 2D arrays (matrices))\n",
    "- Matrix or vector norm: `np.linalg.norm(X,my_norm)`\n",
    "\n",
    "A core task in machine learning is to measure similarity between objects. For example, we would expect that _iris setosa_ samples are 'closer' between them in some sense than they are to _iris versicolor_ samples. One distance measure we can try is the Euclidean distance:\n",
    "\n",
    "$$\\mathrm{dist}(\\mathbf{x}_1, \\mathbf{x}_2)=||\\mathbf{x}_1-\\mathbf{x}_2||_2$$\n",
    "\n",
    "where $\\mathbf{x}_1$ is the vector of measurements for one flower, $\\mathbf{x}_2$ for the other flower, and $|| \\ldots ||_2$ is the $\\ell^2$ norm.\n",
    "\n",
    "If we denote $\\mathbf{d}\\triangleq\\mathbf{x}_1-\\mathbf{x}_2$, we can also use the dot product: \n",
    "\n",
    "$$\\mathrm{dist}(\\mathbf{x}_1, \\mathbf{x}_2)=\\sqrt{\\mathbf{d}\\mathbf{d}^T}$$\n",
    "\n",
    "Now, let's check a few things\n",
    "- Are _iris setosa_ samples really closer to each other than they are to _iris versicolor_ samples?\n",
    "- How does the code look for the equivalence between $\\ell^2$ norm and dot product above?\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e3316e25",
   "metadata": {},
   "outputs": [],
   "source": [
    "# WRITE code to compute Euclidean distance between e.g. the first and second setosa irises.\n",
    "\n",
    "# WRITE same, but between e.g. first setosa and last versicolor\n",
    "\n",
    "# WRITE code to check that you indeed get same results for the l2 norm of the difference vector and the square root of the dot product on the difference vector\n",
    "\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
