"""Some utility functions for the hands-on introduction to maching learning for biomedical data course"""

__version__ = '0.1'
__author__ = 'Jonas Richiardi'

import numpy as np


def print_stat_summary(x: np.array) -> None:
    """Prints min, max, mean, sd for a given array of performance metrics

    Args:
        x: a 1D numpy array

    Returns:
        None
    """

    print(f"Min: {np.min(x):0.3f}, Max: {np.max(x):0.3f}, Mean: {np.mean(x):0.3f}, sd: {np.std(x):0.3f}")

