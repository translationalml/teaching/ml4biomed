{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "c28af036",
   "metadata": {},
   "source": [
    "Kernel methods: the support vector machine (SVM)\n",
    "================================================\n",
    "\n",
    "> Kernel methods are distinguished by their theoretically sound foundation in learning theory and their outstanding empirical results. - Thomas Gärtner\n",
    "\n",
    "In this notebook you will learn to use linear support vector machines, and to use scikit-learn pipelines to have proper evaluation.\n",
    "\n",
    "After completing this notebook, you should be able to\n",
    "\n",
    "* Explain Wahba's representer theorem and verify it empirically in the linear kernel case\n",
    "* Use high-level functions for model evaluations, including simple Pipelines\n",
    "\n",
    "You need to **complete and run** the code blocks that contain a WRITE comment, and if you have time left you can complete the BONUS parts."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9d076f56",
   "metadata": {},
   "source": [
    "---\n",
    "# Setup\n",
    "\n",
    "\n",
    "Let's start by loading the dummy-coded Cleveland Heart Disease data you prepared previously.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "be15b85d",
   "metadata": {},
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "import seaborn as sns\n",
    "from sklearn.preprocessing import StandardScaler\n",
    "\n",
    "# WRITE pandas code to read your saved dataframe (With categoricals already dummy-coded) into a variable called dfc\n",
    "dfc=pd.read_pickle('/Users/Richiardi/_cloudStorage/switchDrive/teaching/ML4BioMed/data/week03/solutions/cleveland_dfc_dc.pkl')\n",
    "\n",
    "\n",
    "dfc.info()\n",
    "\n",
    "# to avoid mistakes let's separate class label and features\n",
    "X=dfc.drop('Class', axis=1)\n",
    "y=dfc.Class\n",
    "\n",
    "# Also generate a pre-scaled version for convenience only. In general don't do this outside of cross-validation.\n",
    "my_scaler=StandardScaler()\n",
    "X_z=pd.DataFrame(my_scaler.fit_transform(X), columns=X.columns.values)\n",
    "X_z.describe()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d83a437a",
   "metadata": {},
   "source": [
    "---\n",
    "# Theory in practice\n",
    "\n",
    "## Duality and the representer theorem\n",
    "\n",
    "Quick recap - the representer theorem ([Grace Wahba 1990](https://epubs.siam.org/doi/book/10.1137/1.9781611970128)) says that the solution of some empirical risk minimization problems written as a loss function plus quadratic regularization can be written as a combination of the training data. This means that in the linear case (so your parameters $\\boldsymbol{\\theta} = \\left(\\mathbf{w}, b \\right)$), rather than learning $D$ weights (and a bias), you can learn (at most) $N$ coefficients ($\\alpha_i \\ldots \\alpha_n)$ and end up with the same $\\mathbf{w}$. \n",
    "\n",
    "The *generalized* representer theorem ([Bernhard Schölkopf et al 2000](http://alex.smola.org/papers/2001/SchHerSmo01.pdf)) says that the same holds true but with combinations of (valid) kernels of the feature vectors, so you have $\\mathbf{w} =\\sum_{i} \\alpha_{i} k\\left(\\cdot, \\mathbf{x}_{i}\\right)$\n",
    "\n",
    "Samples with  $\\alpha_i \\neq 0$ are called *support vectors* and are those samples that are 'important' in defining the decision boundary.\n",
    "\n",
    "Thus, solutions can be found using the primal formulation (finding $\\mathbf{w}$), or the dual formulation (finidng $\\boldsymbol{\\alpha}$). The dual formulation is particularly advantageous when $D \\gg N$.\n",
    "\n",
    "Let's convince ourselves that this works!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1587e15e",
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.svm import SVC\n",
    "\n",
    "# let's pick a 2D feature vector to keep things simple.\n",
    "X_z_small=X_z[['Num_Major_Vessels_Flouro', 'ST_Depression_Exercise']]\n",
    "sns.jointplot(x='Num_Major_Vessels_Flouro', y='ST_Depression_Exercise', hue=y, data=X_z_small)\n",
    "\n",
    "# SVC uses a library called libsvm, while LinearSVC uses a library called liblinear.\n",
    "# liblinear can only use linear kernels, while libsvm is more flexible\n",
    "my_SVC=SVC(kernel='linear', random_state=42, max_iter=5000, C=0.1)\n",
    "\n",
    "# train SVM on whole dataset, no splitting\n",
    "my_SVC.fit(X_z_small,y)\n",
    "\n",
    "# Look at primal coefficients - weight on each feature\n",
    "print(my_SVC.coef_)\n",
    "\n",
    "# Look at dual coefficients - weight on each support vector (sample with non-zero alpha)\n",
    "print(my_SVC.dual_coef_)\n",
    "\n",
    "# confirm that alpha * support_vectors == w\n",
    "print(np.dot(my_SVC.dual_coef_,my_SVC.support_vectors_))\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "43e7c66c",
   "metadata": {},
   "source": [
    "Note that this is only straightforward for linear kernels.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "62380584",
   "metadata": {},
   "source": [
    "--- \n",
    "# Using linear support vector machines with higher-level functions\n",
    "\n",
    "## LinearSVC basics\n",
    "\n",
    "By now the approach is familiar - instantiate the classifier you want (here, a `LinearSVC`), then `fit()`, then `predict()`. \n",
    "Again, the `C` parameter controls the regularization, with smaller C corresponding to more regularisation (and thus smaller weight vector norm).\n",
    "\n",
    "As usual, this should be done in cross-validation as you've seen in the 'evaluation' notebook. You could go back to that notebook and copy-paste from there, \n",
    "and everything would work the same. But scikit-learn offers several convenient high-level functions which can help properly evaluate your models with very compact code. \n",
    "\n",
    "## (almost) proper evaluation in a single line of code\n",
    "\n",
    "Using [`model_selection.cross_val_score()`](https://scikit-learn.org/stable/modules/generated/sklearn.model_selection.cross_val_score.html), we can specify a model, the data to use, the number of cross-validation folds (or the type of cross-validation to perform), and the metric to compute (e.g. `roc_auc` or `accuracy`) with a single function call.\n",
    "\n",
    "Have a quick look at the documentation, and let's try it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c61d5f07",
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.model_selection import cross_val_score\n",
    "from sklearn.svm import LinearSVC\n",
    "\n",
    "# WRITE sklearn code to instantiate a LinearSVC here, using l2 penalty, C=1e-3, max_iter=10000\n",
    "# optimize the primal objective (dual=False) since we have more samples than features\n",
    "my_clf=LinearSVC(penalty='l2', C=1e-3, random_state=42, max_iter=10000, dual=False)\n",
    "#my_clf=LinearSVC(penalty='l1', C=1e-1, random_state=42, max_iter=10000, dual=False)\n",
    "\n",
    "# WRITE sklearn code to perform 10-fold CV (using your instantiated linearSVC), together with the (unstandardized) features and class labels.\n",
    "# You should report classification accuracy.\n",
    "# Store the results in a variable called e.g. my_results\n",
    "# HINT you don't need to split data yourself!\n",
    "my_results=cross_val_score(my_clf, X_z, y, cv=10, scoring='accuracy', n_jobs=1)\n",
    "\n",
    "# WRITE code to display all results, then mean and sd of the accuracy across folds\n",
    "print(my_results)\n",
    "print(f\"avg (sd) acc : {np.mean(my_results)}({np.std(my_results)})\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "267c0f56",
   "metadata": {},
   "source": [
    "You should see something like \n",
    "\n",
    "`[0.8        0.73333333 0.73333333 0.86666667 0.93333333 0.7\n",
    " 0.7        0.75862069 0.65517241 0.75862069]`\n",
    " \n",
    "Be careful when parallelizing (`n_jobs` > 1), this may hide convergence warnings from the optimizer.\n",
    " \n",
    "Try an $\\ell_1$ penalty. You may need to tweak the regularization constant. Try using the standardized version `X_z`. \n",
    " \n",
    "Overall, what's the best accuracy you get ?\n",
    "\n",
    "## Some caveats\n",
    "\n",
    "While using high-level functions is great for quick assessment of a classifier type or feature set, it does make looking into details and interpretation more difficult. Thus, it is generally a good idea to keep in mind the approach where you explicitely wrote out the cross-validation loop by iterating over the output of a CV generator, because you have more opportunities to observe and customize the behaviour of your classifiers.\n",
    "\n",
    "Now, why is this 'almost' proper evaluation? Two main points:\n",
    "- First, by running multiple CV experiments with different parameters over the same dataset (train/validate/train/validate/...), we are ourselves doing 'human overfitting', and we are almost certainly introducing optimistic bias. The immortal Randall Munroe, as usual, has a [great take on this](https://xkcd.com/1838/) that hits close to home... So, ideally we should keep another held-out dataset at hand to report results on, or indeed replicate on data from another source if it is available. At a minimum, we need to report results obtained with, e.g. another regulariser, to show how sensitive our learning problem is to particular design choices.\n",
    "- Second, when we use the pre-standardized version X_z, we of course break the independence between training and testing. We should standardize on-the-fly in each fold. The impact of this varies depending on the dataset size, cross-validation scheme, and feature distributions.\n",
    "\n",
    "Fortunately, scikit-learn offers a nice tool to do everything correctly with minimum typing."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e282166f",
   "metadata": {},
   "source": [
    "## Pipelines \n",
    "\n",
    "[Pipelines](https://scikit-learn.org/stable/modules/generated/sklearn.pipeline.Pipeline.html#sklearn.pipeline.Pipeline) are of the most powerful features in scikit-learn, and allows you to chain processing and modelling steps together into a single object.\n",
    "Then, passing this object to other functions such as `cross_val_score()` will ensure that the pipeline is run everytime on properly separated data.\n",
    "\n",
    "Here are the basic principles:\n",
    "- A Pipeline is instantiated with a list (so, square brackets `[...]`) of steps\n",
    "- Each step is represented as a tuple (with parentheses `(...)`) that has a name (string of your choice) and a call to a transform or estimator: `('my_step_1', MyEstimator(my_param1=1, my_param2=2...))`\n",
    "- The last step in a Pipeline must be an estimator, so that the Pipeline itself will have a `fit()` function (as well as a `.predict()` and a `.score()`)\n",
    "\n",
    "So for example the following is valid:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1c4303dc",
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.model_selection import train_test_split\n",
    "from sklearn.pipeline import Pipeline\n",
    "\n",
    "# single hold-out set. We use the non-standardized version because this will be done on-the-fly in the pipeline\n",
    "X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.20, random_state=42)\n",
    "\n",
    "\n",
    "my_svm_pipe = Pipeline([\n",
    "    ('my_scaler', StandardScaler()),\n",
    "    ('my_svm', LinearSVC(penalty='l2', C=1e-1, random_state=42, max_iter=10000, dual=False))\n",
    "])\n",
    "\n",
    "my_svm_pipe.fit(X_train, y_train)\n",
    "\n",
    "# this will apply the StandardScaler() properly automatically, that is fit on train and transform on test\n",
    "my_svm_pipe.score(X_test, y_test) # will return accuracy"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "279a2219",
   "metadata": {},
   "source": [
    "But you can avoid manually splitting your data by directly passing your pipeline for cross-validation.\n",
    "\n",
    "Let's try it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bc93af80",
   "metadata": {},
   "outputs": [],
   "source": [
    "# WRITE sklearn code to directly obtain cross-validated accuracy (10 folds) from unstandardized X, using your svm pipeline\n",
    "my_results_proper=cross_val_score(my_svm_pipe, X, y, cv=10, scoring='accuracy')\n",
    "\n",
    "# WRITE code to print average and sd of the accuracy\n",
    "print(f\"avg (sd) acc : {np.mean(my_results_proper)}({np.std(my_results_proper)})\")\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3167084a",
   "metadata": {},
   "source": [
    "Results should be very close (but slightly lower) to those above where pre-split standardization was used.\n",
    "\n",
    "So, why all the fuss? First, as mentioned, the impact of cheating for standardization depends on the dataset. Second, and most importantly, this is a way of thinking that should always be present - **only learn using training data**! We'll talk about feature selection next week, and cheating there (called peeking or double-dipping) is a sure-fire way to get wrong results."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "36b6beb2",
   "metadata": {},
   "source": [
    "--- \n",
    "# BONUS \n",
    "\n",
    "## Towards fullly automated preprocessing - on-the-fly categorical encoding\n",
    "\n",
    "Just as we used `StandardScaler()` in a pipeline, we could use `OneHotEncoder()`. How would you make sure that it applies only to categorical columns?\n",
    "\n",
    "`ColumnTransformer` can help. You can try modifying the pipeline example above so that you can preprocess both numerical and categorical variables on the fly.\n",
    "\n",
    "\n",
    "## A look at the Gram matrix for the linear kernel\n",
    "\n",
    "By using a linear kernel $k(\\mathbf{x}_i,\\mathbf{x}_j)=\\mathbf{x}_i^T\\mathbf{x}_j^T$, we are implicitely saying that we believe the dot product is a suitable measure of similarity between samples. So, if you sort samples by class, then compute the Gram matrix $\\mathbf{G}=\\mathbf{X}^T\\mathbf{X}^T$, and visualise it as a heatmap (say, with `plt.imshow()`), same-class blocks (on the block-diagonal of the matrix) should have high values, indicating high similarity, while different-class block (off-block-diagonal) should have low similarity.\n",
    "\n",
    "Try it! \n",
    "\n",
    "Then try it again with the gene expression data (for a partial replication of the Nature paper), and then with the Iris data. Remember to sort by class first.\n",
    "\n",
    "The beauty of kernel methods is that they don't care how you computed $\\mathbf{G}$, so long as it's positive semidefinite. This means that you are free to compute similarity between your objects using any (valid) kernel function you like, and so these objects don't even have to be vectors! You can compute kernels on graphs, sets, and many other mathematical objects.\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
