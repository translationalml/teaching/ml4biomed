"""Dendrogram plotting
source: scikit-learn doc : https://scikit-learn.org/stable/auto_examples/cluster/plot_agglomerative_dendrogram.html
with modification for sklearn 0.23 based on code from Mathew Kallada
"""

import numpy as np

from scipy.cluster.hierarchy import dendrogram
from sklearn.cluster import AgglomerativeClustering
from matplotlib import pyplot as plt


def plot_dendrogram_sk024(model, **kwargs) -> None:
    """
    Plot a dendrogram given a fitted AgglomerativeClustering object
    scikit-learn 0.24 version
    """
    # Create linkage matrix and then plot the dendrogram

    # create the counts of samples under each node
    counts = np.zeros(model.children_.shape[0])
    n_samples = len(model.labels_)
    for i, merge in enumerate(model.children_):
        current_count = 0
        for child_idx in merge:
            if child_idx < n_samples:
                current_count += 1  # leaf node
            else:
                current_count += counts[child_idx - n_samples]
        counts[i] = current_count

    linkage_matrix = np.column_stack([model.children_, model.distances_,
                                      counts]).astype(float)

    # Plot the corresponding dendrogram
    dendrogram(linkage_matrix, **kwargs)


def plot_dendrogram(model, **kwargs) -> None:
    """
    Plot a dendrogram given a fitted AgglomerativeClustering object
    scikit-learn 0.23 version - AgglomerativeClustering has no distances_ attribute.
    
    """

    # Create linkage matrix and then plot the dendrogram

    # create the counts of samples under each node
    counts = np.zeros(model.children_.shape[0])
    n_samples = len(model.labels_)
    for i, merge in enumerate(model.children_):
        current_count = 0
        for child_idx in merge:
            if child_idx < n_samples:
                current_count += 1  # leaf node
            else:
                current_count += counts[child_idx - n_samples]
        counts[i] = current_count
        
    # use uniform distances - Mathew Kallada
    # https://github.com/scikit-learn/scikit-learn/blob/70cf4a676caa2d2dad2e3f6e4478d64bcb0506f7/examples/cluster/plot_hierarchical_clustering_dendrogram.py
    distances = np.arange(model.children_.shape[0])

    linkage_matrix = np.column_stack([model.children_, distances,
                                      counts]).astype(float)

    # Plot the corresponding dendrogram
    dendrogram(linkage_matrix, **kwargs)